<?php

//use Illuminate\Http\Request;

Route::group(['prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group(['midleware' => 'jwt.auth'], function ($router) {


    Route::get('users', 'CustomUserController@all');
    Route::get('users/{id}', 'CustomUserController@get');
    Route::put('users', 'CustomUserController@new');
    Route::post('users/{id}', 'CustomUserController@update');

});
