<?php

namespace App\Http\Controllers;

use App\Http\Models\CustomUser;
use Illuminate\Http\Request;

use App\Http\Requests\CreateCustomUserRequest;

class CustomUserController extends Controller
{
    public function all() {
        $users = CustomUser::all();

        return response()->json([
            "users" => $users
        ], 200);
    }

    public function get($id) {
        $user = CustomUser::whereId($id)->first();

        return response()->json([
            "user" => $user
        ], 200);
    }

    public function new( CreateCustomUserRequest $request) {


        $fields = $request->only(["username", "last_name", "first_name", "email", "password"]);
        $fields['password'] = bcrypt($fields['password']);
        $user = CustomUser::create($fields);

        return response()->json([
            "user" => $user
        ], 200);
    }

    public function update($id, CreateCustomUserRequest $request) {

        $user = CustomUser::whereId($id)->first();
        if ($user !== null){
            $fields = $request->only(["id", "username", "last_name", "first_name", "email", "password"]);

            foreach ($fields as $key=>$value){
                switch ($key){
                    case 'password':
                        if ($user->password != $value){
                            $value = bcrypt($value);
                        }

                        break;
                }
                $user->$key = $value;
            }
            $user->save();

            return response()->json([
                "user" => $user
            ], 200);
        }else
            return response()->json([
                "user" => null
            ], 200);

    }
}
