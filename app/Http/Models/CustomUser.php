<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CustomUser extends Model
{
    protected $guarded = [];
}
