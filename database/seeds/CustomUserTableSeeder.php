<?php
use \App\Http\Models\CustomUser;
use Illuminate\Database\Seeder;


class CustomUserTableSeeder extends Seeder
{
    public function run()
    {
        CustomUser::create(
            [
                'username'=>'admin',
                'email' => 'admin@site.com',
                'password' => bcrypt('12345678'),
                'first_name' => 'foo',
                'last_name' => 'bar'
            ]
        );
    }
}
