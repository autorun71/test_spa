import Home from './components/Home'
import Login from './components/auth/Login'
import UserMain from './components/users/Main';
import UserList from './components/users/List';
import NewUser from './components/users/New';
import EditUser from './components/users/Update';
import User from './components/users/View';

export const routes = [
    {
        path: '/',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/users',
        component: UserMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: UserList
            },
            {
                path: 'new',
                component: NewUser
            },
            {
                path: ':id',
                component: User
            },
            {
                path: ':id/edit',
                component: EditUser
            }

        ]
    }
]